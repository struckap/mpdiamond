import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import * as firebase from 'firebase';

@IonicPage()
@Component({
  selector: 'page-cars',
  templateUrl: 'cars.html',
})
export class CarsPage {
  cars:any = [];
  newCar = { name:'',icon: '', type: '', price: 0, enable: true};
  carTemp: any = [];
  currency: any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public toastCtrl: ToastController) {
  }

  ionViewDidLoad() {
    this.getCarInfo();
  }
  getCarInfo(){
    firebase.database().ref('master_settings/prices/default/').child('currency').on('value', currency => { this.currency = currency.val() });
    firebase.database().ref('master_settings/prices/default/vehicles/').on('value', snapshot=>{
      let tmp = [];
      snapshot.forEach( car =>{
        tmp.push({ key: car.key, ...car.val()});
        return false;
      })
      this.cars = tmp;
    });
  }
  delete(key){
    firebase.database().ref('master_settings/prices/default/vehicles/'+key).remove();
  }
  add(){
    this.newCar.type = (this.newCar.name).toLowerCase().trim().split(' ').join('_');
    console.log(this.newCar);
    firebase.database().ref('master_settings/prices/default/vehicles/'+ this.newCar.type).set(this.newCar).then( data=>{
      this.displayToast("New Car has beed added");
      this.newCar = { name:'',icon: '', type: '', price: 0, enable: true};
    });
  }
  update(i){
    let car = this.cars[i];
    firebase.database().ref('master_settings/prices/default/vehicles/'+car.key).update(car).then(data=>{
      this.displayToast("Updated Successfully");
    }).catch( err=> console.log(err));
  }
  updateCurrency(){
    firebase.database().ref('master_settings/prices/default/').child('currency').set(this.currency);
  }
  displayToast(message){
    this.toastCtrl.create({ message: message, duration: 2000}).present();
  }

  setDefault(){
    firebase.database().ref('master_settings').set({
        "prices" : {
          "default" : {
            "currency" : "$",
            "vehicles" : {
              "sedan" : {
                "enable" : true,
                "icon" : "assets/img/sedan.svg",
                "name" : "Sedan",
                "price" : 0.2,
                "type": "sedan",
                "map_icon": "assets/img/map-sedan.png"
              },
              "suv" : {
                "enable" : true,
                "icon" : "assets/img/suv.svg",
                "name" : "SUV",
                "price" : 0.2,
                "type": "suv",
                "map_icon": "assets/img/map-suv.png"
              },
              "taxi" : {
                "enable" : true,
                "icon" : "assets/img/taxi.svg",
                "name" : "Taxi",
                "price" : 0.4,
                "type": "taxi",
                "map_icon": "assets/img/map-taxi.png"
              }
            }
          }
        }
      }
    )
  }
}
