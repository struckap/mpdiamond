import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { WithdrawRequestsPage } from './withdraw-requests';

@NgModule({
  declarations: [
    WithdrawRequestsPage,
  ],
  imports: [
    IonicPageModule.forChild(WithdrawRequestsPage),
  ],
})
export class WithdrawRequestsPageModule {}
