import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import * as firebase from 'firebase';
import { DriverinfoPage } from '../driverinfo/driverinfo';

@IonicPage()
@Component({
  selector: 'page-withdraw-requests',
  templateUrl: 'withdraw-requests.html',
})
export class WithdrawRequestsPage {
  allrequests: any = [];
  constructor(public navCtrl: NavController, public navParams: NavParams, public modalCtrl: ModalController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad WithdrawRequestsPage');
    this.getAllWithDraw();
  }

  getAllWithDraw() {
    firebase.database().ref('transactions').on('value', requests => {
      let tmp = [];
      requests.forEach(request => {
        tmp.push({ key: request.key, ...request.val() });
        return false;
      });
      this.allrequests = tmp;
      console.log(this.allrequests);
    })
  }
  send(txn){
    firebase.database().ref('drivers/'+txn.userId).child('balance').once('value', data =>{
      let balance = data.val();
      firebase.database().ref('drivers/'+txn.userId).update({ balance: balance - txn.amount }).then( data=>{
        firebase.database().ref('transactions/'+txn.key).update({ status: 'SUCCESS'});
      })
    })
    
  }
  cancel(txnId){
    firebase.database().ref('transactions/'+txnId).update({ status: 'CANCELED'});
  }

  viewDriver(key){
    console.log(key);
    this.modalCtrl.create(DriverinfoPage, { key }).present();
  }
}
